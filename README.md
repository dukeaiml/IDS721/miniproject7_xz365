# mini-project7: Data Processing with Vector Database
> Xingyu, Zhang (NetID: xz365)

## Download And Run Vector Database [Qdrant](https://qdrant.tech/)
```bash
docker pull qdrant/qdrant
docker run -p 6333:6333 -p 6334:6334 -e QDRANT__SERVICE__GRPC_PORT="6334" qdrant/qdrant
```
![qdrant](./assets/qdrant.png)

## Create The Rust Project To Process Data
Run
```bash
Cargo new project_name
cd project_name
```
to create a new rust project and switch to the project's root directory.

### Add dependencies to [Cargo.toml](./miniproject7/Cargo.toml)

```toml
[dependencies]
tokio = { version = "1", features = ["full"] }
serde_json = "1.0"
qdrant-client = "1.8.0"
anyhow = "1.0"
```

### Write Functions To Perform Data Processing in [main.rs](./miniproject7/src/main.rs)

Instructions of applying ```Qdrant``` can be find [here](https://qdrant.tech/documentation/quick-start).

#### Initialize The Client And Create A Collection
```rust
// The Rust client uses Qdrant's GRPC interface to initialize
let mut client = QdrantClient::from_url("http://localhost:6334").build()?;

// Create a collection
let collection_name = "test_collection";

client.
    create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 3,
                distance: Distance::Dot.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    })
    .await?;
```

#### Ingest Data Into Vector Database
```rust
// Function to ingest data into Qdrant
async fn ingest_data(client: &mut QdrantClient, collection_name: &str) -> Result<()> {
    for i in 1..5 {
        let points = vec![
            PointStruct::new(
                i,
                vec![7.0, 2.0 + i as f32, 1.0 - i as f32],
                json!(
                    {"Item": format!("Book{}", i)}
                )
                .try_into()
                .unwrap(),
            ),
        ];
        let _operation_info = client
            .upsert_points_blocking(collection_name, None, points, None)
            .await
            .unwrap();

        println!("Data ingest successful...");
    }
    println!(" ");

    Ok(())
}
```


#### Perform Query And Visualize Results
```rust
// Function to perform a search query in Qdrant
async fn search_query(client: &mut QdrantClient, collection_name: &str) -> Result<()> {
    // Query multiple points from the database
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![7.0, 2.0, 1.0],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Visualize the output for each found point
    for (index, point) in search_result.result.iter().enumerate() {
        println!("Point {} Payload: {:?} Score: {}", index + 1, serde_json::to_string_pretty(&point.payload).unwrap(), point.score);
    }

    Ok(())
}
```

### Results
![result](./assets/result.png)