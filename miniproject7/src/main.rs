use tokio;
use serde_json::json;
use qdrant_client::prelude::*;
use qdrant_client::client::QdrantClient;
use qdrant_client::qdrant::{CreateCollection, PointStruct, SearchPoints};
use qdrant_client::qdrant::{vectors_config::Config, VectorParams, VectorsConfig};
use anyhow::Result;

// Function to ingest data into Qdrant
async fn ingest_data(client: &mut QdrantClient, collection_name: &str) -> Result<()> {
    for i in 1..5 {
        let points = vec![
            PointStruct::new(
                i,
                vec![7.0, 2.0 + i as f32, 1.0 - i as f32],
                json!(
                    {"Item": format!("Book{}", i)}
                )
                .try_into()
                .unwrap(),
            ),
        ];
        let _operation_info = client
            .upsert_points_blocking(collection_name, None, points, None)
            .await
            .unwrap();

        println!("Data ingest successful...");
    }
    println!(" ");

    Ok(())
}

// Function to perform a search query in Qdrant
async fn search_query(client: &mut QdrantClient, collection_name: &str) -> Result<()> {
    // Query multiple points from the database
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![7.0, 2.0, 1.0],
            limit: 3,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Visualize the output for each found point
    for (index, point) in search_result.result.iter().enumerate() {
        println!("Point {} Payload: {:?} Score: {}", index + 1, serde_json::to_string_pretty(&point.payload).unwrap(), point.score);
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    // The Rust client uses Qdrant's GRPC interface to initialize
    let mut client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Create a collection
    let collection_name = "test_collection";

    client.
        create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 3,
                    distance: Distance::Dot.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    // Ingest data into Qdrant
    ingest_data(&mut client, collection_name).await?;

    // Perform a search query
    search_query(&mut client, collection_name).await?;

    Ok(())
}
